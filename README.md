# chrono - A package for analysis of chronobiology
***
A package for chronobiological analysis.
***
## License
MIT
## Acf
Defined in "acf.go", which exports autocorrelation package.
## Corr
In "acf.go". The argument is a list of doublets whose element is `float64`.<br>
The result R fulfills |R| ≤ 1.
## GetQp
Defined in "chi2.go". GetQp(x) returns list of Q<sub>P</sub> used in &chi;<sup>2</sup> periodogram.

### What to do (so far)
1. Define a function which calculates critical region(s) in &chi;<sup>2</sup> periodogram
1. Support analysis of phase-shift (define a function calculating &Delta;&phi;)
