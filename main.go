package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
)

func main() {
	var fp *os.File
	var lines [][]string
	if len(os.Args) < 2 {
		fp = os.Stdin
	} else {
		var err error
		fp, err = os.Open(os.Args[1])
		if err != nil {
			panic(err)
		}
		defer fp.Close()
	}

	reader := csv.NewReader(fp)
	reader.Comma = ','
	reader.LazyQuotes = true
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}
		lines = append(lines, record)
	}
	/* lines = [[s11 s12 s13 ... ] [s21 s22 s23 ... ] ... [sij sij+1 sij+2] ...] where sij is typed string */
}
