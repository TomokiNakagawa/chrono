/*
Package chrono is a package for chronobiological analyses.
*/
package chrono

import "math"

func corr(d [][2]float64) float64 {
	var aveX, aveY, nmr, dnmX, dnmY float64 = 0, 0, 0, 0, 0
	l := len(d)
	for i := 0; i < l; i++ {
		aveX += d[i][0]
		aveY += d[i][1]
	}
	aveX /= float64(l)
	aveY /= float64(l)

	for i := 0; i < l; i++ {
		nmr += (d[i][0] - aveX) * (d[i][1] - aveY)
		dnmX += (d[i][0] - aveX) * (d[i][0] - aveX)
		dnmY += (d[i][1] - aveY) * (d[i][1] - aveY)
	}

	dnmX = math.Sqrt(dnmX)
	dnmY = math.Sqrt(dnmY)

	return (nmr / (dnmX * dnmY))

}

/*
Acf supports recieves a list of float64 ([]float64) as its argument,
and returns its autocorrelation whose format is []float64.
Here, ${return value}[n] is the (auto-)correlation coefficient where data frames are displaced by n.
Thus, the length of return value is smaller than that of the argument list by 1.

Usage:
	x := []float64{ ... }
	r = Acf(x)
	# This will get r = [r_1 r_2 ... r_n], where |r_j| <= 1.
*/
func Acf(x []float64) []float64 {
	res := make([]float64, len(x)-1)
	for i := 0; i < len(x)-1; i++ {
		s := x[i:]
		t := x[:len(x)-i]
		tmp := make([][2]float64, len(s))
		for j := 0; j < len(s); j++ {
			tmp[j] = [2]float64{s[j], t[j]}
		}
		res[i] = corr(tmp)
	}
	return res
}

/*
PhaseShift provides 2 functions, PhaseShift.Corr and PhaseShift.Chi2.
These functions returns the list of 
 - correlation efficient calculated based on autocorrelation function
and 
 - Qp values determined by the method of chi-square periodogram
respectively. Note that these do not return Δφ itself.
*/
type PhaseShift struct {
	Corr func(x []float64) ([float64]) {
		return nil
	}
	Chi2 func(x []float64) ([float64]) {
		return nil
	}
}