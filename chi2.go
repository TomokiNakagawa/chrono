/*
Package chrono is a package for chronobiological analyses.
*/
package chrono

/*
GetQp recieves x []float64 list and returns list of Qp.
When the sampling rate of x is \(Delta)t, the list of Qp becomes
	[Q_\(Delta)t, Q_2\(Delta)t, ...]
*/
func GetQp(x []float64) []float64 {
	varX := variant(x)
	Qp := make([]float64, len(x))
	for i := 0; i < len(x); i++ {
		Qp[i] = float64(len(x)) * variant(getYp(i+1, x)) / varX
	}
	return Qp
}

func variant(x []float64) float64 {
	var sum float64
	aveX := average(x)
	for i := 0; i < len(x); i++ {
		sum += (x[i] - aveX) * (x[i] - aveX)
	}
	return (sum / float64(len(x)))
}

/*
getYp slices x into p lists, and returns averages of its columns.

		x0		x1		x2		...		xn-1 : length = n

	->	 x0			 x1			...		xp-1	r = 0
		 xp			xp+1		...		x2p-1	r = 1
		...
		 xip		xip+1		...		xip-1	r = i
		...
		...			xn-1
	-----------------------------------------
average: Y1		 	Y2		...		Yp

** Even if m mod p != 0, the last row is not discarded **
*/
func getYp(p int, x []float64) []float64 {
	Yp := make([]float64, p)
	rows := len(x) / p
	var tmp []float64
	for i := 0; i < p; i++ {
		if p*rows+i < len(x) {
			tmp = make([]float64, rows+1)
		} else {
			tmp = make([]float64, rows)
		}
		for j := 0; j*p+i < len(x); j++ {
			tmp[j] = x[j*p+i]
		}
		Yp[i] = average(tmp)

	}
	return Yp
}

func average(x []float64) float64 {
	var sum float64
	for i := 0; i < len(x); i++ {
		sum += x[i]
	}
	return (sum / float64(len(x)))
}
